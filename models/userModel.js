const crypto = require('crypto');
const mongoose = require('mongoose');
const validator = require('validator');
const bcrypt = require('bcryptjs');

const userSchema = new mongoose.Schema({
  name: {
    type: String,
    required: [true, 'User name is required']
  },
  email: {
    type: String,
    required: [true, 'Please provide your Email!'],
    unique: true,
    lowercase: true,
    validate: [validator.isEmail, 'Please provide a valid email']
  },
  photo: { type: String, default: 'default.jpg' },
  password: {
    type: String,
    required: [true, 'Please provide password'],
    minlength: 8,
    select: false
  },
  passwordConfirm: {
    type: String,
    required: [true, 'Please provide passwordConfirm'],
    validate: {
      validator: function(el) {
        return el === this.password;
      },
      message: 'Password are not same!'
    }
  },
  passwordChangeAt: Date,
  passwordResetToken: String,
  passwordResetExpires: Date,
  active: { type: Boolean, default: true, select: false },
  role: {
    type: String,
    enum: ['user', 'guide', 'lead-guide', 'admin'],
    default: 'user'
  }
});

userSchema.pre('save', async function(next) {
  if (!this.isModified('password')) return next();
  this.password = await bcrypt.hash(this.password, 12);
  this.passwordConfirm = undefined;
  next();
});

userSchema.pre('save', function(next) {
  if (!this.isModified('password') || this.isNew) return next();
  this.passwordChangeAt = Date.now() - 1000;
  next();
});

userSchema.pre(/^find/, function(next) {
  this.find({ active: { $ne: false } });
  next();
});

userSchema.methods.correctPassword = async function(
  candidatePassword,
  userPassword
) {
  return await bcrypt.compare(candidatePassword, userPassword);
};

userSchema.methods.changedPasswordAfter = function(JWTTimestamp) {
  if (this.passwordChangeAt) {
    const changedTimestamp = parseInt(
      this.passwordChangeAt.getTime() / 1000,
      10
    );
    return JWTTimestamp < changedTimestamp;
  }
  return false;
};

userSchema.methods.createPasswordResetToken = function() {
  const resetToken = crypto.randomBytes(32).toString('hex');
  this.passwordResetToken = crypto
    .createHash('sha256')
    .update(resetToken)
    .digest('hex');
  this.passwordResetExpires = Date.now() + 10 * 60 * 1000; // 10min
  return resetToken;
};

const User = mongoose.model('User', userSchema);

module.exports = User;

// Date.now()의 리턴값은 해당연도의 경과 밀리초.(ex. 1612018800000)
// 하지만, passwordChangeAt:Date로 설정했기에 Date형식으로 변환됨.(ex. Sun Jan 31 2021 00:00:00 GMT+0900)
// 그래서 document를 불러서 instance methods에서는 getTime()함수를 쓸 수 있음.

// passwordChangeAt에 저장된 것은 Sun Jan 31 2021 00:00:00 GMT+0900 형태의 Date object이다.
// 거기서 getTime()으로 받는 시간은 밀리초 단위, 그래서 1000을 나눈다.
// 이 과정에서 (초 단위로 완벽하게 떨어지지 않는 이상)float로 변경이 되기 때문에 parseInt()로 정수형으로 변환해준다.
