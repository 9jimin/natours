/* eslint-disable node/no-unsupported-features/es-syntax */
/* eslint-disable import/prefer-default-export */
/* eslint-disable no-undef */

export const displayMap = locations => {
  mapboxgl.accessToken =
    'pk.eyJ1IjoiOTA5amltaW4iLCJhIjoiY2s4c3RicTJqMGxhejNscWF0NW04bjFjaSJ9.NICT8YXZgqNKdhFT_3eNsw';

  const map = new mapboxgl.Map({
    container: 'map', //id map에 넣는다는 뜻
    style: 'mapbox://styles/909jimin/ck8sthjb51nkn1in1srntxddt', // mapbox token, style
    scrollZoom: false
    // center:[-118,34],
    // zoom:10,
    // interactive:false
  });

  const bounds = new mapboxgl.LngLatBounds();

  locations.forEach(loc => {
    // create maker
    const el = document.createElement('div');
    el.className = 'marker';

    // add marker
    new mapboxgl.Marker({
      element: el,
      anchor: 'bottom'
    })
      .setLngLat(loc.coordinates)
      .addTo(map);

    // add popup
    new mapboxgl.Popup({
      offset: 30
    })
      .setLngLat(loc.coordinates)
      .setHTML(`<p>Day ${loc.day}: ${loc.description}</p>`)
      .addTo(map);

    // extend map bounds to include current location
    bounds.extend(loc.coordinates);
  });

  map.fitBounds(bounds, {
    padding: { top: 200, bottom: 50, left: 100, right: 100 }
  });
};
