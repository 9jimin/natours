/* eslint-disable no-undef */
/* eslint-disable node/no-unsupported-features/es-syntax */
/* eslint-disable import/prefer-default-export */
import axios from 'axios';
import { showAlert } from './alerts';

const stripe = Stripe('pk_test_POkQyr0ZFkRpz6aDgY8UaG6W00Tap3Y3wy');

export const bookTour = async tourId => {
  try {
    // 1. Get checkout session from API'
    const session = await axios(`/api/v1/bookings/checkout-session/${tourId}`);
    // 2. Create checkout form+charge credit card
    await stripe.redirectToCheckout({
      sessionId: session.data.session.id
    });
  } catch (err) {
    console.log(err);
    showAlert('error', err);
  }
};
