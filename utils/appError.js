class AppError extends Error {
  constructor(message, statusCode) {
    super(message);

    this.statusCode = statusCode;
    this.status = `${statusCode}`.startsWith('4') ? 'fail' : 'error';
    this.isOperational = true;

    Error.captureStackTrace(this, this.constructor);
  }
}
module.exports = AppError;

// Error 클래스에 기능을 추가하고 싶을때, extends를 쓴다.
// parent constructor를 부르기 위해 super를 쓴다.
// 그리고 message는 Error의 유일한 parameter이기 때문에 통과시킨다.
