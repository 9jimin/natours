module.exports = fn => {
  return (req, res, next) => {
    fn(req, res, next).catch(next);
  };
};

// (async(req, res, next)=>{}).catch(err=>next(err))
// catch에는 항상 에러가 들어감.
